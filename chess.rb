require 'debugger'

class Chess
  # def initialize
  #   @board = Board.new
  #   play
  # end
  #
  # def play
  # end

end

class Board

  attr_accessor :board, :pieces

  def initialize
    @board = Array.new(8) { Array.new(8) }
    set_board
    @pieces = []
    store_pieces
  end

  def set_board
    #white pieces
    board[7][0] = Rook.new([7,0], :white, self)
    board[7][7] = Rook.new([7,7], :white, self)
    board[7][1] = Knight.new([7,1], :white, self)
    board[7][6] = Knight.new([7,6], :white, self)
    board[7][2] = Bishop.new([7,2], :white, self)
    board[7][5] = Bishop.new([7,5], :white, self)
    board[7][3] = Queen.new([7,3], :white, self)
    board[4][3] = Queen.new([4,3], :white, self) # delete
    board[7][4] = King.new([7,4], :white, self)
    (0..7).each do |x|
      board[6][x] = Pawn.new([6,x], :white, self)
    end

    #black pieces
    board[0][0] = Rook.new([0,0], :black, self)
    board[0][7] = Rook.new([0,7], :black, self)
    board[0][1] = Knight.new([0,1], :black, self)
    board[0][6] = Knight.new([0,6], :black, self)
    board[0][2] = Bishop.new([0,2], :black, self)
    board[0][5] = Bishop.new([0,5], :black, self)
    board[0][3] = Queen.new([0,3], :black, self)
    board[0][4] = King.new([0,4], :black, self)
    (0..7).each do |x|
      board[1][x] = Pawn.new([1,x], :black, self)
    end
  end

  def store_pieces
    (0..1).each do |row|
      (0..7).each do |col|
        pieces << board[row][col]
      end
    end
    (6..7).each do |row|
      (0..7).each do |col|
        pieces << board[row][col]
      end
    end
    pieces << board[4][3]
  end

  def find_king(color)
    pieces.each do |piece|
      return piece if piece.color == color && piece.class == King
    end
  end

  def checked?(victims_color)
    colors_king = find_king(victims_color)
    p colors_king.position
    pieces.each do |piece|
      return true if piece.moves.include?(colors_king.position)
    end

    false
  end

  def move(start_pos, end_pos)

    moving_piece = board[start_pos[0]][start_pos[1]]
    end_piece = board[end_pos[0]][end_pos[1]]

    if moving_piece == nil
      raise ArgumentError.new "Start position is invalid!"
    end
    if !moving_piece.moves.include?(end_pos)
      raise ArgumentError.new "End position is invalid!"
    end

    if end_piece != nil
      pieces.delete(end_piece)
      end_piece.position = nil
    end
    board[end_pos[0]][end_pos[1]] = moving_piece
    moving_piece.position = end_pos
    board[start_pos[0]][start_pos[1]] = nil
  end

  def dup
    new_board = Board.new

    # pieces.each do |piece|
    #   new_board.pieces << piece
    # end

    board.each_with_index do |row, index1|
      row.each_with_index do |piece, index2|
        next if piece == nil
        new_board.board[index1][index2] = piece.dup
      end
    end

    new_board
  end

  def display_board
    board.each do |row|
      row.each do |square|
        print "_" if square.nil?
        print square.class if square
      end
      puts
    end
  end
end

class Piece
  attr_accessor :position
  attr_reader :board, :color

  def initialize(position, color, board)
    @position = position
    @color = color
    @board = board
  end

  def move_into_check?(pos)
    new_board = board.dup

    new_board.move(position, pos)

    p pos
    p position
    p color
    new_board.checked?(color)
  end

  def on_board?(coordinates)
    return false if coordinates[0] < 0 || coordinates[0] > 7
    return false if coordinates[1] < 0 || coordinates[1] > 7
    true
  end

end

class SlidingPiece < Piece

  def moves
    # just returns all the possible moves
    possible_moves = []
    self.move_dirs.each do |directions|
      if directions == :diagonals
        possible_moves += get_diag_coords
      elsif directions == :rows
        possible_moves += get_rows_coords
      end
    end
    possible_moves
  end

  def get_diag_coords
    # returns all moves even if it hits another piece
    # as if it were a blank board
    possible_diag_moves = []
    ne_clear = true
    se_clear = true
    sw_clear = true
    nw_clear = true

    (1..8).each do |shift|
      ne_move = [position[0]-shift,position[1]+shift] # [-1,3]
      se_move = [position[0]+shift,position[1]+shift] # 1,3
      sw_move = [position[0]+shift,position[1]-shift] # 1,1
      nw_move = [position[0]-shift,position[1]-shift] # -1,1

      temp_arr = []
      [ne_move, se_move, sw_move, nw_move].each do |move|
        if move == ne_move && ne_clear == true
          temp_arr << move
        elsif move == se_move && se_clear == true
          temp_arr << move
        elsif move == sw_move && sw_clear == true
          temp_arr << move
        elsif move == nw_move && nw_clear == true
          temp_arr << move
        end
      end
      temp_arr.delete_if do |move|
        !(on_board?(move))
      end
      temp_arr.each do |move|
        current_piece = board.board[move[0]][move[1]]
        if current_piece # there is a piece on this spot
          ne_clear = false if move == ne_move
          se_clear = false if move == se_move
          sw_clear = false if move == sw_move
          nw_clear = false if move == nw_move
          possible_diag_moves << move if current_piece.color != color
        else #no piece at that position
          possible_diag_moves << move
        end
      end

        # on the board and has not been blocked yet
        # check if move position has a piece on it
        # and whether it's ours or the opponents
        #if ours, do not add move and set clear to false
        #if opponent's, add move and set clear to false
    end

    possible_diag_moves
  end



  def get_rows_coords
    possible_row_moves = []
    n_clear = true
    s_clear = true
    w_clear = true
    e_clear = true

    (1..8).each do |shift|
      n_move = [position[0],position[1]+shift]
      s_move = [position[0],position[1]-shift]
      w_move = [position[0]-shift,position[1]]
      e_move = [position[0]+shift,position[1]]

      temp_arr = []
      [n_move, s_move, w_move, e_move].each do |move|
        if move == n_move && n_clear == true
          temp_arr << move
        elsif move == s_move && s_clear == true
          temp_arr << move
        elsif move == w_move && w_clear == true
          temp_arr << move
        elsif move == e_move && e_clear == true
          temp_arr << move
        end
      end

      temp_arr.delete_if do |move|
        !(on_board?(move))
      end

      temp_arr.each do |move|
        current_piece = board.board[move[0]][move[1]]
        if current_piece # there is a piece on this spot
          n_clear = false if move == n_move
          s_clear = false if move == s_move
          e_clear = false if move == e_move
          w_clear = false if move == w_move
          possible_row_moves << move if current_piece.color != color
        else #no piece at that position
          possible_row_moves << move
        end
      end
    end
      possible_row_moves
  end

  def move_dirs
    #throw error
  end

end

class Bishop < SlidingPiece

  def move_dirs
    [:diagonals]
  end
end

class Rook < SlidingPiece

  def move_dirs
    [:rows]
  end
end

class Queen < SlidingPiece

  def move_dirs
    [:diagonals, :rows]
  end
end

class SteppingPiece < Piece

  def moves
    possible_moves = []
    temp_array = []

    self.move_deltas.each do |delta|
      temp_array << [position[0] + delta[0], position[1] + delta[1]]
    end

    temp_array.each do |move|
      if on_board?(move)
        possible_moves << move if !(occupied_by_us?(move))
      end
    end

    possible_moves
  end

  def occupied_by_us?(coordinates)

    if board.board[coordinates[0]][coordinates[1]]
      return true if board.board[coordinates[0]][coordinates[1]].color == color
    else
      false
    end

  end

  def move_deltas
    # throw error
  end

end

class King < SteppingPiece

  def move_deltas
    [[-1,-1], [-1,0], [-1,1], [0,-1], [0,1], [1,-1], [1,0], [1,1]]
  end

end

class Knight < SteppingPiece

  def move_deltas
    [[-1,-2], [-1,2], [1,-2], [1,2], [-2,-1], [-2,1], [2,-1], [2,1]]
  end
end

class Pawn < Piece

  def moves
    possible_moves = []
    forward_shift = -1 if color == :white
    forward_shift = 1 if color == :black

    forward_move = [position[0]+forward_shift,position[1]]
    left_diag_move = [position[0]+forward_shift,position[1]-1]
    right_diag_move = [position[0]+forward_shift,position[1]+1]

    possible_moves << forward_move if check_forward(forward_move)
    possible_moves << left_diag_move if check_diag(left_diag_move)
    possible_moves << right_diag_move if check_diag(right_diag_move)

    possible_moves
  end

  def check_forward(move)
    return true if board.board[move[0]][move[1]] == nil && on_board?(move)
    false
  end

  def check_diag(move)
    return false if board.board[move[0]][move[1]] == nil
    return true if board.board[move[0]][move[1]].color != color
    false
  end

end

# board1 = Board.new
# board1.move([4,3],[1,3])
# board1.move([1,3],[4,3])
# board1.board[0][4].move_into_check?([1,3])
#
# test_board = Board.new
# test_board.set_board
# test_board.board[7][1].moves
# test_board.display_board